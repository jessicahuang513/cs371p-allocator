// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // ------------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * Check that the array is filled with valid blocks
     * and that no two consecutive blocks are free blocks.
     */
    bool valid () const {
        const_iterator b = begin();
        const_iterator e = end();
        while(&(*b) <= &(*e)) {
            // check that sentinels are the same
            int left_sentinel = *b;
            int right_sentinel = *(reinterpret_cast<const int *>((reinterpret_cast<const char *>(&(*b)) + 4 + abs(left_sentinel))));
            if(left_sentinel != right_sentinel) {
                return false;
            }

            // check that no two consecutive blocks are free
            ++b;
            if((const char *)&(*b) < &a[N - 4] && left_sentinel > 0 && *b > 0) {
                return false;
            }
        }
        return true;
    }

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return (lhs._p == rhs._p);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            int block_size = *_p;
            int* new_addr = reinterpret_cast<int *>(reinterpret_cast<char *>(_p) + abs(block_size) + 8);
            _p = new_addr;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            int block_size = *(_p - 1);
            int* new_addr = reinterpret_cast<int *>(reinterpret_cast<char *>(_p) - abs(block_size) - 8);
            _p = new_addr;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return (lhs._p == rhs._p);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            int block_size = *_p;
            const int* new_addr = reinterpret_cast<const int *>(reinterpret_cast<const char *>(_p) + abs(block_size) + 8);
            _p = new_addr;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            int block_size = *(_p - 1);
            const int* new_addr = reinterpret_cast<const int *>(reinterpret_cast<const char *>(_p) - abs(block_size) - 8);
            _p = new_addr;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    my_allocator() {
        if(N < (sizeof(T) + (2 * sizeof(int)))) {
            std::bad_alloc exception;
            throw exception;
        }

        for(int i = 0; i < N; i++)
            a[i] = 0;

        reinterpret_cast<int *>(a)[0] = N - 8;
        reinterpret_cast<int *>(a)[(N/4) - 1] = N - 8;

        assert(valid());
    }
    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator (size_t n) {
        if(N < (sizeof(T) + (2 * sizeof(int)))) {
            std::bad_alloc exception;
            throw exception;
        }

        for(int i = 0; i < N; i++)
            a[i] = 0;

        reinterpret_cast<int *>(a)[0] = N - 8;
        reinterpret_cast<int *>(a)[(N/4) - 1] = N - 8;

        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        assert(valid());

        iterator b = begin();
        iterator e = end();

        while(&(*b) <= &(*e)) {
            if(*b > 0 && *b >= (int)(n * sizeof(T))) {
                // whole block is allocated
                if(*b < (int)( (n * sizeof(T)) + sizeof(T) + 8)) {
                    int size = *b;
                    *b = -size;
                    int * right_sentinel = reinterpret_cast<int *>((reinterpret_cast<char *>(&(*b)) + size + 4));
                    *(right_sentinel) = -size;
                    return reinterpret_cast<pointer>(&(*b) + 1);
                }
                else {
                    int size = *b;
                    int sentinel_val = -n * sizeof(T);
                    *b = sentinel_val;

                    int* right = reinterpret_cast<int *>(reinterpret_cast<char *>(&(*b)) + (-sentinel_val) + 4);
                    *(right) = sentinel_val;

                    int free_block_size = size + sentinel_val - 8;
                    int* next_left = right + 1;
                    *(next_left) = free_block_size;

                    int* next_right = reinterpret_cast<int*>(reinterpret_cast<char*>(&(*next_left)) + free_block_size + 4);
                    *(next_right) = free_block_size;
                    return reinterpret_cast<pointer>(&(*b) + 1);
                }
            }
            ++b;
        }

        std::bad_alloc exception;
        throw exception;

        return nullptr;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * takes in a pointer to the beginning of a datablock
     * and a size n, referring to the number of Ts to deallocate
     */
    void deallocate (pointer block, size_type n) {
        if((char *) block < &a[4] || (char *)block > &a[N - 1]) {
            std::invalid_argument exception("Invalid argument");
            throw exception;
        }

        // get sentinel value
        block = reinterpret_cast<pointer>(reinterpret_cast<int *>(block) - 1);

        iterator beg = begin();
        ++beg;

        int size = n * sizeof(T);
        int sentinel_size = -*(reinterpret_cast<int *>(block));
        assert(size <= sentinel_size);

        bool entire_block_dealloc = false;
        int * right_sentinel; // free block right sentinel

        // deallocate entire block
        if(size == sentinel_size || (size + sizeof(T) + 8) > sentinel_size) {
            entire_block_dealloc = true;
            size = sentinel_size;
            *(reinterpret_cast<int *>(block)) = sentinel_size;
            right_sentinel = reinterpret_cast<int*>(reinterpret_cast<char*>(block) + sentinel_size + 4);
            *right_sentinel = sentinel_size;
        }
        else {
            *(reinterpret_cast<int *>(block)) = size;
            right_sentinel = reinterpret_cast<int*>(reinterpret_cast<char*>(block) + size + 4);
            *right_sentinel = size;

            //create new sentinels
            int * next_left_sent = right_sentinel + 1;
            int leftover = -(sentinel_size - size - 8);
            *next_left_sent = leftover;
            int* next_right_sent = reinterpret_cast<int*>(reinterpret_cast<char*>(block) + sentinel_size + 4);
            *next_right_sent = leftover;
        }
        // coalesce free blocks
        iterator b = iterator(reinterpret_cast<int *>(block));

        bool left_block_found = false;
        bool right_block_found = false;
        int* left_block;
        int* right_block;

        if((char *)&(*b) != &a[0]) {
            --b;
            left_block = &(*b);
            left_block_found = true;
            ++b;
        }
        if(entire_block_dealloc && &(*b) < &(*(end()))) {
            ++b;
            right_block = &(*b);
            right_block_found = true;
        }

        int* l = (int *)block;

        if(left_block_found && *left_block > 0) {
            size += *left_block + 8;
            *left_block = size;
            l = left_block;
            *right_sentinel = size;
        }
        if(right_block_found && *right_block > 0) {
            int* r = reinterpret_cast<int *>(reinterpret_cast<char*>(right_block) + *right_block + 4);
            size += *right_block + 8;
            *r = size;
            *l = size;
        }
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        int sentinel = reinterpret_cast<int *>(a)[N/4 - 1];
        int last_block = N - (4 + 4 + abs(sentinel));
        assert(last_block >= 0);
        assert(last_block < (int)N);
        return iterator(&(*this)[last_block]);
    }

    // ---
    // end
    // ---s

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        int sentinel = reinterpret_cast<const int *>(a)[N/4 - 1];
        int last_block = N - (4 + 4 + abs(sentinel));
        assert(last_block >= 0);
        assert(last_block < (int)N);
        return const_iterator(&(*this)[last_block]);
    }
};

#endif // Allocator_h
