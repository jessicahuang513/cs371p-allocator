# CS371p: Object-Oriented Programming Allocator Repo

## Partner 1

- Name: Jessica Huang
- EID: jh65898
- GitLab ID: jessicahuang513
- HackerRank ID: jesshuang513


## Partner 2

- Name: Rahul Ramaswamy
- EID: rrr3228
- GitLab ID: rahulramaswamy
- HackerRank ID: rahul_ramaswamy

------

- Git SHA: 182a046e9f81545d37c8949413a0e36d62ee8600
- GitLab Pipelines: https://gitlab.com/jessicahuang513/cs371p-allocator/pipelines
- Estimated completion time: 10
- Actual completion time: 6