// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <sstream>
#include <vector>

#include "Allocator.hpp"

using namespace std;

void allocate_eval(const vector<int>& v, ostream& w) {
    my_allocator <double, 1000> allocator(1);
    for(int i = 0; i < (int)v.size(); i++) {
        assert(v[i] != 0);
        if(v[i] > 0) {
            allocator.allocate(v[i]);
        }
        else if (v[i] < 0) {
            int count = 0;
            // find busy block denoted by -i
            my_allocator<double, 1000>::iterator b = allocator.begin();
            my_allocator<double, 1000>::iterator e = allocator.end();
            while(&(*b) <= &(*e) && count < -v[i]) {
                if(*b < 0)
                    ++count;
                if(count == -v[i])
                    break;
                ++b;
            }
            if(count != -v[i])
                cout << "busy block not found." << endl;
            else {
                size_t s = (size_t)((-*b)/8);
                allocator.deallocate((double *)(&(*b) + 1), s);
            }
        }
    }

    // create iterators and print out resulting array
    my_allocator<double, 1000>:: iterator b = allocator.begin();
    my_allocator<double, 1000>:: iterator e = allocator.end();
    while(&(*b) <= &(*e)) {
        w << *b;
        ++b;
        if(&(*b) <= &(*e))
            w << " ";
    }
}

void allocate_solve(istream& r, ostream& w) {
    int cases;
    r >> cases;

    string blank;
    getline(r, blank);
    getline(r, blank);

    for(int i = 0; i < cases; ++i) {
        string input;
        vector<int> requests;
        while (getline(r, input)) {
            if(input.empty()) {
                break;
            }
            else {
                istringstream tmp(input);
                int n;
                tmp >> n;
                requests.push_back(n);
            }
        }

        allocate_eval(requests, w);
        if(i != (cases - 1))
            w << "\n";
    }
}

// ----
// main
// ----

int main () {
    allocate_solve(cin, cout);
    return 0;
}

